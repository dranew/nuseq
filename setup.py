from Cython.Build import cythonize
from setuptools import find_packages, Extension, setup

import Cython.Compiler.Options
Cython.Compiler.Options.annotate = True

import numpy as np
import os
import pysam


def get_extension(name):
    path = os.path.join(*name.split('.')) + '.pyx'

    return Extension(
        name,
        [path, ],
        include_dirs=pysam.get_include() + [np.get_include(), ],
        define_macros=pysam.get_defines()
    )

extensions = [
    get_extension('nuseq.features.extractors.core'),
    get_extension('nuseq.features.extractors.bam'),
    get_extension('nuseq.features.extractors.fasta'),
    get_extension('nuseq.features.extractors.constants'),
    get_extension('nuseq.features.extractors.data_frame')
]


setup(
    name='nuseq',
    version='0.1.1',
    author='Andrew Roth',
    author_email='andrewjlroth@gmail.com',
    license='GPL3 Licenses',
    ext_modules=cythonize(extensions),
    packages=find_packages(),
    entry_points={'console_scripts': ['nuseq = nuseq.ui:main']},
)
