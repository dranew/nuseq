# Cython imports
from cpython cimport array as c_array
from libc.math cimport log
from pysam.libcalignmentfile cimport AlignmentFile
from pysam.libcalignedsegment cimport AlignedSegment

cimport cython
cimport numpy as np

# Python imports
from collections import OrderedDict

import array
import numpy as np
import pandas as pd

# Local Cython imports
from .constants cimport C_NUCLEOTIDES, NUCLEOTIDES, NUM_NUCLEOTIDES
from .core cimport FeatureExtractor

cdef class BamFeatureExtractor(FeatureExtractor):
    cdef AlignmentFile bam

    def __init__(self, bam):
        self.bam = bam

    def extract_features_from_coords(self, cython.str chrom, int beg, int end, bint count_duplicates=False, int min_bqual=-1, int min_mqual=-1):
        ref_length = self.bam.lengths[self.bam.references.index(chrom)]

        min_index = 0

        max_index = ref_length

        if beg < min_index:
            print 'Beginning index {0} is less than {1}. Setting to {1}.'.format(beg, min_index)

            beg = min_index

        if end > max_index:
            print 'End index {0} is greater than {1}, the length of the chromosome. Setting to {1}.'.format(end, max_index)

            end = max_index

        if (end < min_index) or (beg > max_index) or ((end - beg) <= 0):
            return None

        cdef int length = end - beg

        cdef _BamDataResult result = _BamDataResult(length)

        for read in self.bam.fetch(reference=chrom, start=beg, end=end):

            if read.is_unmapped:
                continue

            if read.is_qcfail:
                continue

            if read.is_duplicate and not count_duplicates:
                continue

            if read.is_secondary:
                continue

            if read.is_supplementary:
                continue

            if read.mapping_quality <= min_mqual:
                continue

            # Read info
            add_read_counts(beg, end, read, result, min_bqual=min_bqual)

        compute_read_count_entropy(result)

        return result.to_data_frame(chrom, beg, end)

cdef class _AlleleResult:
    cdef np.uint64_t[:, :] bqual
    cdef np.uint64_t[:, :] forward_counts
    cdef np.uint64_t[:, :] distance
    cdef np.uint64_t[:, :] indel_counts
    cdef np.uint64_t[:, :] mqual
    cdef np.uint64_t[:, :] reverse_counts

    def __init__(self, length):
        self.bqual = np.zeros([length, NUM_NUCLEOTIDES], dtype=np.uint64)

        self.distance = np.zeros([length, NUM_NUCLEOTIDES], dtype=np.uint64)

        self.forward_counts = np.zeros([length, NUM_NUCLEOTIDES], dtype=np.uint64)

        self.indel_counts = np.zeros([length, NUM_NUCLEOTIDES], dtype=np.uint64)

        self.mqual = np.zeros([length, NUM_NUCLEOTIDES], dtype=np.uint64)

        self.reverse_counts = np.zeros([length, NUM_NUCLEOTIDES], dtype=np.uint64)

    def to_data_frame(self):
        counts = np.asarray(self.forward_counts) + np.asarray(self.reverse_counts)

        df = OrderedDict()

        self._add_to_df(df, 'counts', counts)

        self._add_to_df(df, 'forward_counts', self.forward_counts)

        self._add_to_df(df, 'reverse_counts', self.reverse_counts)

        self._add_to_df(df, 'bqual', self.bqual)

        self._add_to_df(df, 'mqual', self.mqual)

        self._add_to_df(df, 'indel', self.indel_counts)

        self._add_to_df(df, 'distance', self.distance)

        return pd.DataFrame(df, columns=df.keys())

    def _add_to_df(self, df, key, result):
        result = np.asarray(result)

        for nuc_index, nuc in enumerate(NUCLEOTIDES):
            df['{0}_{1}'.format(nuc, key)] = result[:, nuc_index]

        df['total_{0}'.format(key)] = result.sum(axis=1)

cdef class _PositionResult:
    cdef np.uint64_t[:] ambiguous_counts
    cdef np.uint64_t[:] deletion_counts
    cdef np.float64_t[:] entropy
    cdef np.uint64_t[:] insertion_counts
    cdef np.uint64_t[:] indel_counts

    def __init__(self, length):
        self.ambiguous_counts = np.zeros(length, dtype=np.uint64)

        self.deletion_counts = np.zeros(length, dtype=np.uint64)

        self.entropy = np.zeros(length, dtype=np.float64)

        self.insertion_counts = np.zeros(length, dtype=np.uint64)

        self.indel_counts = np.zeros(length, dtype=np.uint64)

    def to_data_frame(self):
        df = OrderedDict()

        self._add_to_df(df, 'ambiguous_counts', self.ambiguous_counts)

        self._add_to_df(df, 'deletion_counts', self.deletion_counts)

        self._add_to_df(df, 'insertion_counts', self.insertion_counts)

        self._add_to_df(df, 'num_reads_with_indel', self.indel_counts)

        self._add_to_df(df, 'entropy', self.entropy)

        return pd.DataFrame(df, columns=df.keys())

    def _add_to_df(self, df, key, result):
        df[key] = np.asarray(result)

cdef class _BamDataResult:
    cdef _AlleleResult allele
    cdef unsigned int length
    cdef _PositionResult position

    def __init__(self, length):
        self.length = length

        self.allele = _AlleleResult(length)

        self.position = _PositionResult(length)

    def to_data_frame(self, chrom, beg, end):
        df = pd.concat([self.allele.to_data_frame(), self.position.to_data_frame()], axis=1)

        df.insert(0, 'chrom', chrom)

        df.insert(1, 'coord', pd.np.arange(beg, end))

        df['chrom'] = df['chrom'].astype(str)

        return df

cdef struct ReadInfo:
    bint has_indel
    bint is_reverse

    unsigned int del_count
    unsigned int ins_count
    unsigned int length
    unsigned int mqual

cdef ReadInfo parse_read_info(AlignedSegment read):
    '''
    Parse read info into C struct for fast access.
    '''

    cdef ReadInfo info

    cdef int cigar_op_value
    cdef int cigar_op_len

    info.is_reverse = read.is_reverse

    info.length = read.query_length

    info.mqual = read.mapping_quality

    # Check for indels
    info.del_count = 0

    info.ins_count = 0

    info.has_indel = 0

    for cigar_op_value, cigar_op_len in read.cigar:
        if cigar_op_value == 1:
            info.ins_count += 1

        elif cigar_op_value == 2:
            info.del_count += 1

    if (info.del_count > 0) or (info.ins_count > 0):
        info.has_indel = 1

    else:
        info.has_indel = 0

    return info

cdef add_read_counts(int beg, int end, AlignedSegment read, _BamDataResult result, int min_bqual):
    '''
    Update results for all positions covered by the read.
    '''

    cdef char base
    cdef c_array.array quality
    cdef int nuc_index, pos_index
    cdef int qpos, refpos
    cdef ReadInfo read_info
    cdef char * seq

    quality = read.query_qualities

    seq = < bytes > read.seq

    read_info = parse_read_info(read)

    for qpos, refpos in read.get_aligned_pairs(True):
        if qpos is not None and refpos is not None and beg <= refpos < end:
            if (quality[qpos] <= min_bqual):
                continue

            pos_index = refpos - beg

            base = seq[qpos]

            if base == 'N':
                result.position.ambiguous_counts[pos_index] += 1

            else:
                for nuc_index in range(NUM_NUCLEOTIDES):
                    if base == C_NUCLEOTIDES[nuc_index]:
                        result.allele.bqual[pos_index, nuc_index] += quality[qpos]

                        result.allele.mqual[pos_index, nuc_index] += read_info.mqual

                        if read_info.is_reverse:
                            result.allele.reverse_counts[pos_index, nuc_index] += 1

                            result.allele.distance[pos_index, nuc_index] += read_info.length - qpos

                        else:
                            result.allele.forward_counts[pos_index, nuc_index] += 1

                            result.allele.distance[pos_index, nuc_index] += qpos

                        if read_info.has_indel:
                            result.allele.indel_counts[pos_index, nuc_index] += 1

    for refpos in range( < int > read.reference_start, < int > read.reference_end):
        if refpos < beg:
            continue

        if refpos >= end:
            break

        pos_index = refpos - beg

        result.position.deletion_counts[pos_index] += read_info.del_count

        result.position.insertion_counts[pos_index] += read_info.ins_count

        result.position.indel_counts[pos_index] += read_info.has_indel

cdef compute_read_count_entropy(_BamDataResult result):
    '''
    Compute entropy of position in a BAM file.
    '''
    cdef float prob
    cdef int nuc_index, pos_index
    cdef unsigned int count, total

    for pos_index in range(result.length):
        total = 0

        for nuc_index in range(NUM_NUCLEOTIDES):
            total += result.allele.forward_counts[pos_index, nuc_index] + \
                result.allele.reverse_counts[pos_index, nuc_index]

        if total > 0:
            for nuc_index in range(NUM_NUCLEOTIDES):
                count = result.allele.forward_counts[pos_index, nuc_index] + \
                    result.allele.reverse_counts[pos_index, nuc_index]

                if count > 0:
                    prob = count / < float > total

                    result.position.entropy[pos_index] -= log(prob) * prob
