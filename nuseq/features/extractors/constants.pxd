cimport cython

cpdef cython.str NUCLEOTIDES
cpdef unsigned int NUM_NUCLEOTIDES
cpdef float LOG_NUM_NUCLEOTIDES
cdef unsigned A_INDEX
cdef unsigned C_INDEX
cdef unsigned G_INDEX
cdef unsigned T_INDEX

cdef char * C_NUCLEOTIDES
