'''
Created on Nov 13, 2015

@author: Andrew Roth
'''
from __future__ import division

from scipy.misc import logsumexp as log_sum_exp
from scipy.special import gammaln as log_gamma

import numpy as np


def multinomial_log_pdf(x, p):
    return log_multinomial_coefficient(x) + np.sum(x[np.newaxis, :, :] * np.log(p)[:, :, np.newaxis], axis=0)


def log_multinomial_coefficient(x):
    return log_factorial(x.sum()) - log_factorial(x).sum()


def log_factorial(x):
    return log_gamma(x + 1)


def compute_multinomial_emission(df):
    counts = np.array([
        df['ref_forward_counts_normal'],
        df['ref_reverse_counts_normal'],
        df['alt_forward_counts_normal'],
        df['alt_reverse_counts_normal'],
        df['ref_forward_counts_tumour'],
        df['ref_reverse_counts_tumour'],
        df['alt_forward_counts_tumour'],
        df['alt_reverse_counts_tumour']
    ])

    # prior
    ep = 0.001

    prob = np.array([
        [1 / 6, 1 / 6, ep, ep, 1 / 6, 1 / 6, 1 / 6, 1 / 6],
        [1 / 6, 1 / 6, ep, ep, ep, ep, 1 / 6, 1 / 6],
        [1 / 6, 1 / 6, ep, ep, 1 / 6, 1 / 6, 0.1, 0.1],
        [1 / 8, 1 / 8, 1 / 8, 1 / 8, 1 / 8, 1 / 8, 1 / 8, 1 / 8],
        [ep, ep, 1 / 4, 1 / 4, ep, ep, 1 / 4, 1 / 4],
        [1 / 3, 1 / 3, ep, ep, ep, ep, 1 / 3, ep],
        [1 / 3, 1 / 3, ep, ep, ep, ep, ep, 1 / 3],
        [1 / 5, 1 / 5, ep, ep, 1 / 5, 1 / 5, 1 / 5, ep],
        [1 / 5, 1 / 5, ep, ep, 1 / 5, 1 / 5, ep, 1 / 5],
        [1 / 4, 1 / 4, ep, ep, 1 / 4, 1 / 4, ep, ep]
    ])

    prob = prob / prob.sum(axis=1)[:, np.newaxis]

    # output
    result = multinomial_log_pdf(counts, prob)

    result = result - log_sum_exp(result, axis=0)

    result = np.exp(result)

    return result[0] + result[1] + result[2]


def compute_xentropy(df):
    normal_base_qualities = np.atleast_2d(
        df[['A_bqual_normal', 'C_bqual_normal', 'G_bqual_normal', 'T_bqual_normal']].values)

    tumour_base_qualities = np.atleast_2d(
        df[['A_bqual_tumour', 'C_bqual_tumour', 'G_bqual_tumour', 'T_bqual_tumour']].values)

    normal_base_qualities_total = df['total_bqual_normal'].values

    tumour_base_qualities_total = df['total_bqual_tumour'].values

    # Compute normal probs
    normal_base_probability = np.zeros(normal_base_qualities.shape)

    indices = (normal_base_qualities_total != 0)

    if indices.sum() > 0:
        normal_base_probability[indices] = normal_base_qualities[
            indices] / normal_base_qualities_total[indices][:, np.newaxis]

    normal_base_probability[normal_base_probability == 0] = np.exp(-7)

    # Compute tumour probs
    tumour_base_probability = np.zeros(tumour_base_qualities.shape)

    indices = (tumour_base_qualities_total != 0)

    if indices.sum() > 0:
        tumour_base_probability[indices] = tumour_base_qualities[
            indices] / tumour_base_qualities_total[indices][:, np.newaxis]

    # Return entropy
    return -1 * np.sum(np.log(normal_base_probability) * tumour_base_probability, axis=1)
