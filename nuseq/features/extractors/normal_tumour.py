'''
Created on Nov 13, 2015

@author: Andrew Roth
'''
from __future__ import division

import numpy as np
import pandas as pd

from .bam import BamFeatureExtractor
from .core import FeatureExtractor
from .data_frame import get_alt_base, get_base_values
from .fasta import FastaFeatureExtractor
from .math import compute_multinomial_emission, compute_xentropy

nucleotides = ['A', 'C', 'G', 'T']


class NormalTumourFeatureExtractor(FeatureExtractor):

    def __init__(
        self,
        normal_bam,
        tumour_bams,
        ref_genome_fasta,
        features=None,
        filter_non_somatic=False,
        min_normal_depth=0,
        min_tumour_depth=0
    ):

        self.normal_bam_extractor = BamFeatureExtractor(normal_bam)

        self.tumour_bam_extractors = []

        for tumour_bam in tumour_bams:
            self.tumour_bam_extractors.append(BamFeatureExtractor(tumour_bam))

        self.ref_genome_fasta_extractor = FastaFeatureExtractor(ref_genome_fasta)

        self.filter_non_somatic = filter_non_somatic

        self.features = features

        self.min_normal_depth = min_normal_depth

        self.min_tumour_depth = min_tumour_depth

        self._compute_required_featres()

    def _compute_required_featres(self):
        self._single_sample_features = set()

        if self.features is not None:
            for feature in self.features:
                for suffix in ['_normal_tumour', '_tumour_normal']:
                    if feature.endswith(suffix):
                        self._single_sample_features.add(feature.replace(suffix, ''))

                is_normal_feature = feature.endswith('_normal') and (not feature.endswith('_tumour_normal'))

                is_tumour_feature = feature.endswith('_tumour') and (not feature.endswith('_normal_tumour'))

                if is_normal_feature or is_tumour_feature:
                    feature = '_'.join(feature.split('_')[:-1])

                    self._single_sample_features.add(feature)

    def extract_features_from_position_list(self, positions):
        ref_df = []

        normal_df = []

        tumour_df = []

        for chrom, coord in positions:
            ref_df.append(self.ref_genome_fasta_extractor.extract_features_from_position(chrom, coord))

            normal_df.append(self.normal_bam_extractor.extract_features_from_position(chrom, coord))

            pos_tumour_df = []

            for tumour_bam_extractor in self.tumour_bam_extractors:
                pos_tumour_df.append(tumour_bam_extractor.extract_features_from_position(chrom, coord))

            if len(self.tumour_bam_extractors) > 1:
                pos_tumour_df = pd.concat(pos_tumour_df, axis=0)

                pos_tumour_df = pos_tumour_df.groupby(['chrom', 'coord']).sum().reset_index()

            else:
                pos_tumour_df = pos_tumour_df[0]

            tumour_df.append(pos_tumour_df)

        normal_df = pd.concat(normal_df)

        tumour_df = pd.concat(tumour_df)

        ref_df = pd.concat(ref_df)

        return self._compute_df(normal_df, tumour_df, ref_df)

    def extract_features_from_coords(self, chrom, beg, end):
        ref_df = self.ref_genome_fasta_extractor.extract_features_from_coords(chrom, beg, end)

        normal_df = self.normal_bam_extractor.extract_features_from_coords(chrom, beg, end)

        tumour_df = []

        for tumour_bam_extractor in self.tumour_bam_extractors:
            tumour_df.append(tumour_bam_extractor.extract_features_from_coords(chrom, beg, end))

        if len(self.tumour_bam_extractors) > 1:
            tumour_df = pd.concat(tumour_df, axis=0)

            tumour_df = tumour_df.groupby(['chrom', 'coord']).sum().reset_index()

        else:
            tumour_df = tumour_df[0]

        return self._compute_df(normal_df, tumour_df, ref_df)

    def _compute_df(self, normal_df, tumour_df, ref_df):
        if (normal_df is None) or (tumour_df is None) or (ref_df is None):
            return None

        # Filter 0 depth
        if self.filter_non_somatic:
            indices = (normal_df['total_counts'] >= self.min_normal_depth) & (
                tumour_df['total_counts'] >= self.min_tumour_depth)

            normal_df = normal_df[indices]

            tumour_df = tumour_df[indices]

        if normal_df.empty or tumour_df.empty:
            return None

        # Add ref base info
        normal_df = pd.merge(ref_df[['chrom', 'coord', 'ref_base']], normal_df, on=['chrom', 'coord'])

        tumour_df = pd.merge(ref_df[['chrom', 'coord', 'ref_base']], tumour_df, on=['chrom', 'coord'])

        # Get alt bases
        alt_base = get_alt_base(
            tumour_df['ref_base'].values,
            'ACGT',
            tumour_df[['A_counts', 'C_counts', 'G_counts', 'T_counts']].values
        )

        # Add alt base information
        normal_df.insert(3, 'alt_base', alt_base)

        tumour_df.insert(3, 'alt_base', alt_base)

        # Filter positions without variants
        if self.filter_non_somatic:
            indices = normal_df['alt_base'] != 'N'

            normal_df = normal_df[indices]

            tumour_df = tumour_df[indices]

        if normal_df.empty or tumour_df.empty:
            return None

        # Compute derived BAM features
        normal_df = self._compute_single_sample_features(normal_df)

        tumour_df = self._compute_single_sample_features(tumour_df)

        # Join BAM data
        df = pd.merge(
            normal_df, tumour_df, on=['chrom', 'coord', 'ref_base', 'alt_base'], suffixes=('_normal', '_tumour'))

        # Compute paired features features
        df = self._compute_paired_features(df)

        # Join all data
        df = pd.merge(ref_df, df, on=['chrom', 'coord', 'ref_base'])

        df = df.reset_index()

        df = df.drop('index', axis=1)

        df = df.set_index(['chrom', 'coord', 'ref_base', 'alt_base'])

        df = df.astype(pd.np.float64)

        df = df.reset_index()

        if self.features is not None:
            df = df[self.features]

        return df

    def _compute_single_sample_features(self, df, eps=1e-5):

        def use_feature(feature):
            return (self.features is None) or (feature in self._single_sample_features)

        _convert_to_ref_alt(df)

        for feature in df.columns:
            if feature in ['chrom', 'coord', 'ref_base', 'alt_base']:
                continue

            if feature != 'total_counts':
                derived_feature = feature + '_total_counts_ratio'

                if use_feature(derived_feature):
                    df[derived_feature] = df[feature] / df['total_counts']

        return df

    def _compute_paired_features(self, df, eps=1e-5):

        def use_feature(feature):
            return (self.features is None) or (feature in self.features)

        for feature in df.columns:
            if not feature.endswith('_tumour'):
                continue

            normal_feature = feature.replace('_tumour', '_normal')

            tumour_feature = feature

            normal_tumour_feature = normal_feature + '_tumour'

            if use_feature(normal_tumour_feature):
                df[normal_tumour_feature] = df[normal_feature] / (df[tumour_feature] + eps)

            tumour_normal_feature = tumour_feature + '_normal'

            if use_feature(tumour_normal_feature):
                df[tumour_normal_feature] = df[tumour_feature] / (df[normal_feature] + eps)

        if use_feature('somatic_probability_paired'):
            df['somatic_probability_paired'] = compute_multinomial_emission(df)

        if use_feature('xentropy'):
            df['xentropy'] = compute_xentropy(df)

        return df


def _convert_to_ref_alt(df):
    for allele in ['ref', 'alt']:
        bases = df['{0}_base'.format(allele)].values

        for feature in ['forward_counts', 'reverse_counts', 'bqual', 'distance', 'mqual', 'counts']:
            key = '{0}_{1}'.format(allele, feature)

            cols = []

            for base in nucleotides:
                cols.append('{0}_{1}'.format(base, feature))

            values = df[cols].values.astype(np.uint64)

            df[key] = get_base_values(bases, values)
