# Cython imports
cimport cython
cimport numpy as np

# Python imports
import numpy as np

# Local Cython imports
from .constants cimport NUCLEOTIDES, NUM_NUCLEOTIDES
from .core cimport FeatureExtractor


def get_alt_base(np.ndarray ref_bases, cython.str nucleotides, np.uint64_t[:, :] counts):
    cdef int length = ref_bases.shape[0]

    cdef result = []

    cdef cython.str ref_base

    cdef int i, n, max_alt_counts, best_nuc_index

    for i in range(length):
        max_alt_count = 0

        ref_base = ref_bases[i]

        best_nuc_index = -1

        for n in range(NUM_NUCLEOTIDES):
            if ref_base == nucleotides[n]:
                continue

            if counts[i, n] > max_alt_count:
                max_alt_count = counts[i, n]

                best_nuc_index = n

        if best_nuc_index == -1:
            result.append('N')

        else:
            result.append(nucleotides[best_nuc_index])

    return np.array(result)


def get_base_values(np.ndarray bases, np.uint64_t[:, :] values):
    cdef int length = bases.shape[0]

    cdef np.uint64_t[:] result = np.zeros(length, dtype=np.uint64)

    cdef char * base
    cdef char * nucleotides = <bytes > NUCLEOTIDES

    cdef int i, n

    for i in range(length):
        for n in range(NUM_NUCLEOTIDES):
            base = bases[i]

            if base[0] == nucleotides[n]:
                result[i] += values[i, n]

    return np.asarray(result)
