# Cython imports
from libc.math cimport log
from pysam.libcfaidx cimport FastaFile

cimport cython
cimport numpy as np

# Python imports
from collections import OrderedDict

import array
import numpy as np
import pandas as pd

# Local Cython imports
from .constants cimport C_NUCLEOTIDES, NUCLEOTIDES, NUM_NUCLEOTIDES, LOG_NUM_NUCLEOTIDES, C_INDEX, G_INDEX
from .core cimport FeatureExtractor

cdef class FastaFeatureExtractor(FeatureExtractor):
    cdef FastaFile fasta

    def __init__(self, fasta):
        self.fasta = fasta

    def extract_features_from_coords(self, chrom, beg, end, window_size=500):
        '''
        0 based coordinates
        '''
        ref_length = self.fasta.get_reference_length(chrom)

        min_index = 0

        max_index = ref_length

        if beg < min_index:
            print 'Beginning index {0} is less than {1}. Setting to {1}.'.format(beg, min_index)

            beg = min_index

        if end > max_index:
            print 'End index {0} is greater than {1}, the length of the chromosome. Setting to {1}.'.format(end, max_index)

            end = max_index

        if (end < min_index) or (beg > max_index) or ((end - beg) <= 0):
            return None

        entropy, gc_content = compute_entropy_and_gc_content(self.fasta, chrom, beg, end, window_size)

        df = pd.DataFrame(
            {
                'ref_base': list(self.fasta.fetch(chrom, beg, end)),
                'entropy': entropy,
                'gc_content': gc_content,
                'forward_homopolymer_length': get_forward_homopolymer_count(self.fasta, chrom, beg, end),
                'reverse_homopolymer_length': get_reverse_homopolymer_count(self.fasta, chrom, beg, end),
            },
            columns=('ref_base', 'entropy', 'gc_content', 'forward_homopolymer_length', 'reverse_homopolymer_length')
        )

        df.insert(0, 'chrom', chrom)

        df.insert(1, 'coord', pd.np.arange(beg, end))

        df['chrom'] = df['chrom'].astype(str)

        return df

cdef class SlidingWindowCounter:
    cdef np.uint64_t[:] counts
    cdef unsigned int pos
    cdef char * seq
    cdef Py_ssize_t seq_len
    cdef unsigned int window_beg
    cdef unsigned int window_end
    cdef unsigned int window_size

    def __init__(self, seq, window_size):
        self.counts = np.zeros(4, dtype=np.uint64)

        self.pos = 0

        self.seq = < bytes > seq

        self.seq_len = len(seq)

        self.window_size = window_size

        self._update_window_coords()

    def __iter__(self):
        return self

    def __next__(self):
        self.cnext()

        return self.counts

    cdef cnext(self):
        if self.pos == 0:
            self._init_counts()

        elif self.pos >= self.seq_len - self.window_size:
            raise StopIteration()

        else:
            self._update_counts()

        self.pos += 1

        self._update_window_coords()

    cdef _init_counts(self):
        cdef char base
        cdef int n
        cdef int window_index

        for window_index in range(self.window_beg, self.window_end):
            base = self.seq[window_index]

            for n in range(NUM_NUCLEOTIDES):
                if base == C_NUCLEOTIDES[n]:
                    self.counts[n] += 1

    cdef _update_counts(self):
        cdef char base
        cdef int n

        # Decrement from beginning
        base = self.seq[self.window_beg - 1]

        for n in range(NUM_NUCLEOTIDES):
            if base == C_NUCLEOTIDES[n]:
                self.counts[n] -= 1

        # Increment from end
        base = self.seq[self.window_end - 1]

        for n in range(NUM_NUCLEOTIDES):
            if base == C_NUCLEOTIDES[n]:
                self.counts[n] += 1

    cdef _update_window_coords(self):
        self.window_beg = self.pos

        self.window_end = self.pos + self.window_size + 1


@cython.cdivision(True)
cdef tuple compute_entropy_and_gc_content(FastaFile fasta, cython.str chrom, int beg, int end, int window_size):
    cdef SlidingWindowCounter counter
    cdef np.uint64_t[:] counts
    cdef cython.str seq
    cdef int beg_pad, end_pad, index, window_beg, window_end, total
    cdef float prob

    cdef int half_window_size = window_size // 2
    cdef int region_length = end - beg
    cdef int ref_length = fasta.get_reference_length(chrom)

    cdef np.float64_t[:] entropy = np.zeros(region_length, dtype=np.float64)
    cdef np.float64_t[:] gc_content = np.zeros(region_length, dtype=np.float64)

    window_beg = beg - half_window_size

    window_end = end + half_window_size

    if window_beg < 0:
        beg_pad = abs(window_beg)

        window_beg = 0

    else:
        beg_pad = 0

    if window_end > ref_length:
        end_pad = window_end - ref_length

        window_end = ref_length

    else:
        end_pad = 0

    seq = fasta.fetch(chrom, window_beg, window_end)

    seq = 'N' * beg_pad + seq + 'N' * end_pad

    counter = SlidingWindowCounter(seq, window_size)

    for index in range(region_length):
        counter.cnext()

        counts = counter.counts

        total = 0

        for n in range(NUM_NUCLEOTIDES):
            total += counts[n]

        if total > 0:
            for n in range(NUM_NUCLEOTIDES):
                prob = counts[n] / < float > total

                if prob > 0:
                    entropy[index] -= (log(prob) / LOG_NUM_NUCLEOTIDES) * prob

            gc_content[index] = (counts[C_INDEX] + counts[G_INDEX]) / < float > total

    return entropy, gc_content

cdef class FastaFileProxy:
    cdef FastaFile fasta

    def __init__(self, fasta):
        self.fasta = fasta

    cdef unsigned int get_reference_length(self, cython.str chrom):
        return self.fasta.get_reference_length(chrom)

    cdef cython.str fetch(self, cython.str chrom, int beg, int end):
        pass

cdef class FastaFileForwardProxy(FastaFileProxy):
    cdef cython.str fetch(self, cython.str chrom, int beg, int end):
        return self.fasta.fetch(chrom, beg, end)

cdef class FastaFileReverseProxy(FastaFileProxy):
    cdef cython.str fetch(self, cython.str chrom, int beg, int end):
        cdef unsigned int ref_length = self.get_reference_length(chrom)

        cdef unsigned int rev_beg = ref_length - end

        cdef unsigned int rev_end = ref_length - beg

        return self.fasta.fetch(chrom, rev_beg, rev_end)[::-1]

cdef class HomopolymerCounter(object):
    cdef unsigned int count
    cdef cython.str chrom
    cdef unsigned int beg
    cdef unsigned int end
    cdef FastaFileProxy fasta
    cdef unsigned int ref_length

    cdef unsigned int _buf_beg
    cdef unsigned int _buf_end
    cdef unsigned int _buf_size
    cdef unsigned int _min_buf_size
    cdef unsigned int _pos
    cdef cython.str _seq
    cdef unsigned int _seq_pos

    def __init__(self, chrom, beg, end, fasta, min_buffer_size=64):
        self.chrom = chrom

        self.beg = beg

        self.end = end

        self.fasta = fasta

        self.ref_length = self.fasta.get_reference_length(chrom)

        self.count = 1

        self._buf_beg = beg

        self._buf_size = min_buffer_size

        self._min_buf_size = min_buffer_size

        self._pos = beg

        self._seq_pos = 0

        self._grow_buffer()

    def __iter__(self):
        return self

    def __next__(self):
        self.cnext()

        return self.count

    cdef cnext(self):
        if self._pos >= self.end:
            raise StopIteration()

        elif self.count > 1:
            self.count -= 1

        else:
            self._update_count()

        self._pos += 1

    cdef _update_count(self):
        cdef char base
        cdef char next_base

        cdef char * seq = < bytes > self._seq

        while True:
            if self._seq_pos + 1 >= self._buf_size:
                if self._can_grow_buffer():
                    self._grow_buffer()

                else:
                    break

            base = seq[self._seq_pos]

            next_base = seq[self._seq_pos + 1]

            self._seq_pos += 1

            if base == next_base:
                self.count += 1

            else:
                break

    cdef bint _can_grow_buffer(self):
        if self._buf_end < self.ref_length:
            return 1

        else:
            return 0

    cdef _grow_buffer(self):
        if self.count > 1:
            self._buf_size *= 2

        else:
            self._buf_size = self._min_buf_size

        self._buf_beg = self._buf_beg + self._seq_pos

        self._buf_end = self._buf_beg + self._buf_size

        if self._buf_end > self.ref_length:
            self._buf_end = self.ref_length

            self._buf_size = self._buf_end - self._buf_beg

        self._seq = self.fasta.fetch(self.chrom, self._buf_beg, self._buf_end)

        self._seq_pos = 0

cdef np.uint64_t[:] get_forward_homopolymer_count(FastaFile fasta, str chrom, int beg, int end):

    cdef int region_length = end - beg

    cdef np.uint64_t[:] counts = np.zeros(region_length, dtype=np.uint64)

    cdef HomopolymerCounter counter = HomopolymerCounter(chrom, beg, end, FastaFileForwardProxy(fasta))

    for index in range(region_length):
        counter.cnext()

        counts[index] = counter.count

    return counts

cdef np.uint64_t[:] get_reverse_homopolymer_count(FastaFile fasta, str chrom, int beg, int end):

    cdef unsigned int ref_length = fasta.get_reference_length(chrom)

    cdef int region_length = end - beg

    cdef np.uint64_t[:] counts = np.zeros(region_length, dtype=np.uint64)

    cdef HomopolymerCounter counter = HomopolymerCounter(chrom, ref_length - end, ref_length - beg, FastaFileReverseProxy(fasta))

    cdef int index

    for index in range(region_length):
        counter.cnext()

        counts[region_length - index - 1] = counter.count

    return counts
