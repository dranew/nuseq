'''
Created on Dec 18, 2015

@author: andrew
'''


def chunker(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))


def get_coords(bam, region):
    valid_region, chrom_index, beg, end = bam.parse_region(region=region)

    coords = []

    if valid_region == 0:
        chroms = bam.references

        for chrom in chroms:
            _, chrom_index, beg, end = bam.parse_region(region=chrom)

            chrom = bam.references[chrom_index]

            end = bam.lengths[chrom_index]

            coords.append((chrom, beg, end))

    else:
        chrom = bam.references[chrom_index]

        end = min(end, bam.lengths[chrom_index])

        coords.append((chrom, beg, end))

    return coords


def iter_coords(beg, end, step_size=int(1e6)):
    lside_interval = range(beg, end + 1, step_size)

    rside_interval = range(beg + step_size, end + step_size, step_size)

    for split_beg, split_end in zip(lside_interval, rside_interval):
        yield split_beg, min(split_end, end)

status_map = {
    'wildtype': 0,
    'germline': 1,
    'indel': 2,
    'somatic': 3
}
