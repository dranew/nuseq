'''
Created on Nov 13, 2015

@author: Andrew Roth
'''
from sklearn.linear_model import ElasticNetCV
from sklearn.ensemble import RandomForestClassifier

import os
import pandas as pd
import pysam
import sklearn.externals.joblib as joblib
import yaml

from nuseq.features.extractors import NormalTumourFeatureExtractor
from nuseq.io import NuseqVcfWriter
from nuseq.training_data import DataSetBuilder
from nuseq.utils import chunker, get_coords, iter_coords, status_map

#=======================================================================================================================
# Extract training data
#=======================================================================================================================


def build_training_data(args):
    with open(args.in_file) as fh:
        config = yaml.load(fh)

    if args.target_features_file is not None:
        with open(args.target_features_file) as fh:
            target_features = yaml.load(fh)

    else:
        target_features = None

    builder = DataSetBuilder(
        config,
        args.out_file,
        args.store_position_information,
        target_features
    )

    builder.build_training_data()

    builder.close()

#=======================================================================================================================
# Feature selection
#=======================================================================================================================


def select_features(args):
    features = pd.read_hdf(args.in_file, 'features')

    labels = pd.read_hdf(args.in_file, 'labels')

    features.index = range(features.shape[0])

    labels.index = range(labels.shape[0])

    indices = ((features['total_counts_normal'] != 0) & (features['total_counts_tumour'] != 0))

    features = features[indices]

    labels = labels[indices]

    # Fit
    X = features.values

    y = labels.values

    X = (X - X.mean(axis=0)) / X.std(axis=0)

    alphas = [0.0001, 0.0003, 0.0005, 0.0007, 0.0009, 0.002, 0.004, 0.01, 0.05]

    l1_ratio = [.1, 0.25, .3]

    clf = ElasticNetCV(
        alphas=alphas,
        l1_ratio=l1_ratio,
        normalize=False,
        fit_intercept=False
    )

    clf.fit(X, y)

    # Save non-zero features
    features = list(features.columns[clf.coef_ != 0])

    with open(args.out_file, 'w') as out_fh:
        yaml.dump(features, out_fh, default_flow_style=False)


def extract_features(args):
    with open(args.target_features_file) as fh:
        target_features = yaml.load(fh)

    in_store = pd.HDFStore(args.in_file, mode='r')

    out_store = pd.HDFStore(args.out_file, mode='w', complevel=9, complib='blosc')

    out_store.append('labels', in_store['labels'])

    out_store.append('features', in_store['features'][target_features])

    if 'positions' in in_store.keys():
        out_store.append('positions', in_store['positions'])

    in_store.close()

    out_store.close()

#=======================================================================================================================
# Train
#=======================================================================================================================


def train_model(args):
    if args.target_features_file is not None:
        with open(args.target_features_file) as fh:
            target_features = yaml.load(fh)

    else:
        target_features = None

    features_df = pd.read_hdf(args.in_file, 'features')

    if target_features is not None:
        features_df = features_df[target_features]

    labels_df = pd.read_hdf(args.in_file, 'labels')

    indices = (features_df.isnull().sum(axis=1) == 0).values

    features_df = features_df[indices]

    labels_df = labels_df[indices]

    clf = RandomForestClassifier(random_state=0, n_estimators=3000, n_jobs=-1)

    clf.fit(features_df.values, labels_df.values)

    clf.n_jobs = 1

    out_dict = {'features': list(features_df.columns), 'classifier': clf}

    joblib.dump(out_dict, args.out_file, compress=9)

#=======================================================================================================================
# Classify
#=======================================================================================================================


def classify(args):
    filter_cols = ['num_reads_with_indel_total_counts_ratio_tumour', ]

    position_cols = ['chrom', 'coord', 'ref_base', 'alt_base']

    position_cols.extend(['{0}_counts_normal'.format(x) for x in ['ref', 'alt', 'A', 'C', 'G', 'T']])

    position_cols.extend(['{0}_counts_tumour'.format(x) for x in ['ref', 'alt', 'A', 'C', 'G', 'T']])

    model = joblib.load(args.model_file)

    clf = model['classifier']

    features = model['features']

    hdf_store = pd.HDFStore(args.out_file, mode='w', complevel=9, complib='blosc')

    normal_bam = pysam.AlignmentFile(args.normal_bam_file, 'rb')

    tumour_bams = []

    for file_name in args.tumour_bam_files:
        tumour_bams.append(pysam.AlignmentFile(file_name, 'rb'))

    ref_genome_fasta = pysam.FastaFile(args.ref_genome_fasta_file)

    coords = get_coords(tumour_bams[0], args.region)

    extractor = NormalTumourFeatureExtractor(
        normal_bam,
        tumour_bams,
        ref_genome_fasta,
        features=list(set(features) | set(filter_cols) | set(position_cols)),
        filter_non_somatic=True,
        min_normal_depth=args.min_normal_depth,
        min_tumour_depth=args.min_tumour_depth,
    )

    out_tables = {
        'positions': position_cols + ['{0}_probability'.format(x) for x in status_map],
        'filters': filter_cols
    }

    if args.store_features:
        out_tables['features'] = features

    for chrom, beg, end in coords:
        print 'Classifying region: {0}:{1}-{2}'.format(chrom, beg + 1, end)

        for split_beg, split_end in iter_coords(beg, end, step_size=args.chunk_size):
            print 'Classifying chunk: {0}:{1}-{2}'.format(chrom, split_beg + 1, split_end)

            df = extractor.extract_features_from_coords(chrom, split_beg, split_end)

            if df is None:
                continue

            df['coord'] = df['coord'] + 1

            for status in status_map:
                col = '{0}_probability'.format(status)

                df[col] = pd.np.nan

            num_rows = df.shape[0]

            classifier_chunk_size = int(1e4)

            for indices in chunker(range(num_rows), classifier_chunk_size):
                features_df = df.ix[indices, features]

                probs = clf.predict_proba(features_df.values)

                for status in status_map:
                    col = '{0}_probability'.format(status)

                    df.ix[indices, col] = probs[:, status_map[status]]

            for status in status_map:
                col = '{0}_probability'.format(status)

                assert df[col].isnull().sum() == 0

            df = df[df['somatic_probability'] >= args.min_somatic_probability]

            hdf_store.append_to_multiple(out_tables, df, selector='positions')

    normal_bam.close()

    for tumour_bam in tumour_bams:
        tumour_bam.close()

    ref_genome_fasta.close()

    hdf_store.close()

#=======================================================================================================================
# VCF
#=======================================================================================================================


def write_vcf(args):
    if args.out_file.endswith('.gz'):
        vcf_file = args.out_file.replace('.gz', '')

        compress = True

    else:
        vcf_file = args.out_file

        compress = False

    writer = NuseqVcfWriter(vcf_file, indel_threshold=args.indel_threshold)

    hdf_store = pd.HDFStore(args.in_file, mode='r')

    # Check for empty files
    if len(hdf_store.keys()) > 0:
        positions_iter = hdf_store.select_as_multiple(
            ['positions', 'filters'],
            where=['somatic_probability >= args.min_somatic_probability'],
            selector='positions',
            iterator=True,
            chunksize=1000
        )

        for df in positions_iter:
            for _, row in df.iterrows():
                writer.write_record(
                    row['chrom'],
                    row['coord'],
                    row['ref_base'],
                    row['alt_base'],
                    row[['{0}_counts_normal'.format(x) for x in writer.nucleotides]].astype(int),
                    row[['{0}_counts_tumour'.format(x) for x in writer.nucleotides]].astype(int),
                    row['germline_probability'],
                    row['indel_probability'],
                    row['somatic_probability'],
                    row['num_reads_with_indel_total_counts_ratio_tumour']
                )

    hdf_store.close()

    writer.close()

    if compress:
        pysam.tabix_compress(vcf_file, args.out_file, force=True)

        pysam.tabix_index(args.out_file, force=True, preset='vcf')

        os.unlink(vcf_file)
